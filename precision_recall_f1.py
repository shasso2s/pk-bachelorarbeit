
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
def calcPrecisionRecallF1(OUTPUT_DF_RESULTS,tn_key,fn_key,fp_key,tp_key,NEW_MODEL_NEW,filename):
    df_results=pd.read_json(OUTPUT_DF_RESULTS)
    curr_tn = df_results[tn_key] #hier liegen alle tn werte drin
    curr_fn = df_results[fn_key]
    curr_fp = df_results[fp_key]
    curr_tp = df_results[tp_key]

    all_precision = []
    all_recall = []
    all_f1 = []
    
    #calculate prec, recall and f1 for all above values in curr_tn etc.:
    for i in range(0, len(curr_tn)):

        if(curr_tp[i]==0):
            curr_precision=0
            curr_recall=0
            curr_f1=0
        else:
            curr_precision = curr_tp[i] / (curr_tp[i] + curr_fp[i])
            curr_recall = curr_tp[i] / (curr_tp[i] + curr_fn[i])
            curr_f1 = 2 * ((curr_precision * curr_recall) / (curr_precision + curr_recall))
        #append to all prec etc.:
        all_precision.append(curr_precision)
        all_recall.append(curr_recall)
        all_f1.append(curr_f1)

    zipped_precision_recall_f1=list(zip(all_precision,all_recall,all_f1))
    dt_precision_recall_f1=pd.DataFrame(zipped_precision_recall_f1,columns=['precision','recall','F1'])
    dt_precision_recall_f1.to_json(NEW_MODEL_NEW+"/"+filename+".json")
    return dt_precision_recall_f1

def plot_precision_recall_f1(OUTPUT_PRECISION_RECALL_F1,NEW_MODEL_NEW_DIR,filename):
    dt_precision_recall_f1=pd.read_json(OUTPUT_PRECISION_RECALL_F1)
    fig, ax = plt.subplots(1,3,figsize=(20,5))
    ax[0].plot(np.array(dt_precision_recall_f1['precision']))
    ax[1].plot(np.array(dt_precision_recall_f1['recall']))
    ax[2].plot(np.array(dt_precision_recall_f1['F1']))
    ax[0].set_xlabel("steps")
    ax[0].set_ylabel('precision')
    ax[1].set_xlabel("steps")
    ax[1].set_ylabel('recall')
    ax[2].set_xlabel("steps")
    ax[2].set_ylabel('F1')
    plt.savefig(NEW_MODEL_NEW_DIR+'/'+filename+".png", dpi=300, bbox_inches='tight')

