
from datetime import datetime
from time import time
from utils import tensor_from_numpy
from utils import tensordataset
from utils import get_iter_spectograms_dataset
from convnet import ConvNet
from train import train_model
from train_validate import train_model_
import torch
import pandas as pd
import argparse
from DenseNet import DenseNet
from ResNet import ResNet
from Inception import Inception
from params import Params
import utils
from store_model import store_model_stats

from select_model import choose_model

import os

OUTPUT_DIR_TRAIN_CSV='dt_parkinson_split/df_train.csv'
OUTPUT_DIR_VALID_CSV='dt_parkinson_split/df_valid.csv'
OUTPUT_DIR_TEST_CSV='dt_parkinson_split/df_test.csv'

OUTPUT_DIR_LOADED_TRAIN='loaded_data/dataDicTrain'
OUTPUT_DIR_LOADED_VALID='loaded_data/dataDicvalid'
OUTPUT_DIR_LOAED_TEST='loaded_data/dataDictest'




SAMPLE_RATE_TRAIN=22050
SAMPLE_RATE_VALID=22050
SAMPLE_RATE_TEST=22050

BATCH_SIZE=8
NUM_CLASS=2

parser = argparse.ArgumentParser()
parser.add_argument("--config_path", type=str)



def main():
    args = parser.parse_args()
    params = Params(args.config_path)
    device='cpu'
    LEARNING_RATE=0.1
    NUM_EPOCHS=300

    model=choose_model()
    optimizer_model=torch.optim.Adam(list(model.parameters()),lr=LEARNING_RATE)
    criterion_model=torch.nn.CrossEntropyLoss()
    scheduler_model =torch.optim.lr_scheduler.StepLR(optimizer_model, step_size=1, gamma=0.6)

    datadictrain = pd.read_pickle(OUTPUT_DIR_LOADED_TRAIN)
    datadictvalid = pd.read_pickle(OUTPUT_DIR_LOADED_VALID)
    datadictest = pd.read_pickle(OUTPUT_DIR_LOAED_TEST)
    
    train_time_series_train,train_labels=tensor_from_numpy(datadictrain['audio_time_series'],datadictrain['audio_labels'])
    valid_time_series_valid,val_labels=tensor_from_numpy(datadictvalid['audio_time_series'],datadictvalid['audio_labels'])
    test_time_series_test,test_labels=tensor_from_numpy(datadictest['audio_time_series'],datadictest['audio_labels'])

    tensor_dataset_train=tensordataset(train_time_series_train,train_labels)
    tensor_dataset_valid=tensordataset(valid_time_series_valid,val_labels)
    tensor_dataset_test=tensordataset(test_time_series_test,test_labels)

    audio_datasets = {"train": tensor_dataset_train, 
                    "valid": tensor_dataset_valid, 
                    "test": tensor_dataset_test}
    dataset_sizes = {x: len(audio_datasets[x]) for x in ["train", "valid", "test"]}
    
    iter_spectogram_dataset_train = get_iter_spectograms_dataset(tensor_dataset_train,SAMPLE_RATE_TRAIN,BATCH_SIZE)
    iter_spectogram_dataset_valid = get_iter_spectograms_dataset(tensor_dataset_valid,SAMPLE_RATE_VALID, BATCH_SIZE)
    iter_spectogram_dataset_test = get_iter_spectograms_dataset(tensor_dataset_test,SAMPLE_RATE_TEST, BATCH_SIZE)

    iter_spectograms_dict = { 'train' : iter_spectogram_dataset_train, 
                            'valid': iter_spectogram_dataset_valid,
                            'test': iter_spectogram_dataset_test}
    #define the model:

    # define the hypreparameters

    dt_train_results,dt_valid_results,model=train_model_(model=model,
    dataloaders=iter_spectograms_dict,
    dataset_sizes=dataset_sizes,
    criterion=criterion_model,
    optimizer=optimizer_model,
    scheduler=scheduler_model,
    NUM_EPOCHS=NUM_EPOCHS,
    device=device)
    
    save_model=store_model_stats(dt_train_results,dt_valid_results,model)
    #df_train_loss_acc(train_loss_list,train_acc_list,NEW_MODEL_NEW_DIR)
    









datadic_train=main()






