from copyreg import pickle
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data as data_utils
import librosa
import os
import pandas as pd
import numpy as np
import pickle
import bz2
import os

OUTPUT_DF_TRAIN='dt_parkinson_split/df_train.csv'
OUTPUT_DF_VALID='dt_parkinson_split/df_valid.csv'
OUTPUT_DF_TEST='dt_parkinson_split/df_test.csv'

BATCH_SIZE = 32
train_sr = 22050
val_sr = 22050
test_sr = 22050
AUDIO_LENGTH=4
OUTPUT_DIR_LOADED_DATA='loaded_data'

try:
    if(os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_DIR_LOADED_DATA))):
        print("Directory  %s is already exist" % OUTPUT_DIR_LOADED_DATA)
    
    else:
        os.mkdir(OUTPUT_DIR_LOADED_DATA)
        print("Directory %s created successfully"% OUTPUT_DIR_LOADED_DATA)
except OSError as error:
    print()
    

    

def get_fixed_audio_len(wav, sr,AUDIO_LENGTH ):
        if wav.shape[0] < AUDIO_LENGTH * sr:
            wav = np.pad(wav, int(np.ceil((AUDIO_LENGTH * sr - wav.shape[0])/2)), mode = 'reflect')
        wav = wav[:AUDIO_LENGTH * sr]
        return wav

def load_data(OUTPUT_DATA_DIR,AUDIO_LENGTH):
    df=pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_DATA_DIR))
    audio_time_series = []
    sample_rates = []
    labels = []     
    tot = len(df)
    curr = 0
    for idx in df.index: 
        try:
            audio_pfad = str(df['audio_path'][idx])
            temp = audio_pfad
            wav, sr = librosa.load(str(temp))
            wav = get_fixed_audio_len(wav, sr,AUDIO_LENGTH)
            audio_time_series.append(wav)
            sample_rates.append(sr)
            label=df['professional-diagnosis'][idx]
            if label is not None:
                labels.append(label)

        except Exception:
            print("Couldn't read file", df['audio_path'][idx])

    dataDictionary={'audio_time_series': np.stack(audio_time_series, axis = 0),
            'sample_rates':np.array(sample_rates),
            'labels':np.array(labels)
    }
    return dataDictionary

def store_loaded_data(dataDictionary,filename):
    dataDic={'audio_time_series':dataDictionary['audio_time_series'],
            'audio_labels':dataDictionary['labels']  }
    try:
        if(os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_DIR_LOADED_DATA,filename))):
             print("%s is already exist"% filename)
        else:
            outfile=open(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_DIR_LOADED_DATA,filename),'wb')
            pickle.dump(dataDic,outfile)
            outfile.close()

            infile=open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
            OUTPUT_DIR_LOADED_DATA,filename),'rb')
            dataDic=pickle.load(infile)
            infile.close()
        
            print(" %s created successfully" % filename)
    except OSError as error:
         print()
    return dataDic

dataDic_train=load_data(OUTPUT_DF_TRAIN,AUDIO_LENGTH)
dataDic_valid=load_data(OUTPUT_DF_VALID,AUDIO_LENGTH)
dataDic_test=load_data(OUTPUT_DF_TEST,AUDIO_LENGTH)

store_loaded_data(dataDic_train,filename='dataDicTrain')
store_loaded_data(dataDic_valid,filename='dataDicvalid')
store_loaded_data(dataDic_test,filename='dataDictest')








