 

from datetime import datetime
from time import time
import os
from train_validate import train_model_
import torch

from plot_acc_loss import plot_df_train_loss_acc
from plot_acc_loss import plot_df_valid_loss_acc
from precision_recall_f1 import calcPrecisionRecallF1
from precision_recall_f1 import plot_precision_recall_f1

 # save our model to new directory:

def store_model_stats(dt_train_results,dt_valid_results,model):
    now = datetime.now()
    timestamp=str(now.year)+"-"+str(now.month)+"-"+str(now.day)+"-"+str(now.hour)+"-"+str(now.minute)+"-"+str(now.second)
    OUTPUT_TRAIN_PATH='train_results\model_version_'
    NEW_MODEL_NEW_DIR=OUTPUT_TRAIN_PATH+timestamp
    try:
        if(os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)),NEW_MODEL_NEW_DIR))):
            print("Directory  %s is already exist" % NEW_MODEL_NEW_DIR)
        else:
            os.makedirs(NEW_MODEL_NEW_DIR)
            print("Directory %s created successfully"% NEW_MODEL_NEW_DIR)
    except OSError as error:
        print()
    CURRENT_MODEL_OUTPUT_PATH=NEW_MODEL_NEW_DIR+"/model_version_"+timestamp+".pth"
    
    torch.save(model.state_dict(),CURRENT_MODEL_OUTPUT_PATH)

    STORE_TRAIN_RESULTS=NEW_MODEL_NEW_DIR+'/dt_train_results.json'
    STORE_VALID_RESULTS=NEW_MODEL_NEW_DIR+'/dt_valid_results.json'

    dt_train_results.to_json(STORE_TRAIN_RESULTS)
    dt_valid_results.to_json(STORE_VALID_RESULTS)

    plot_df_train_loss_acc(STORE_TRAIN_RESULTS,NEW_MODEL_NEW_DIR,'plot_train_loss_acc')
    plot_df_valid_loss_acc(STORE_VALID_RESULTS,NEW_MODEL_NEW_DIR,'plot_valid_loss_acc')

    train_precision_recall_f1=calcPrecisionRecallF1(STORE_TRAIN_RESULTS,'train_tn','train_fn','train_fp','train_tp',NEW_MODEL_NEW_DIR,'train_precision_recall_f1')
    valid_precision_recall_f1=calcPrecisionRecallF1(STORE_VALID_RESULTS,'valid_tn','valid_fn','valid_fp','valid_tp',NEW_MODEL_NEW_DIR,'valid_precision_recall_f1')
    
    OUTPUT_TRAIN_PRECISION_RECALL_F1=NEW_MODEL_NEW_DIR+'/train_precision_recall_f1.json'
    OUTPUT_VALID_PRECISION_RECALL_F1=NEW_MODEL_NEW_DIR+'/valid_precision_recall_f1.json'
    plot_train_precison_recall_f1=plot_precision_recall_f1(OUTPUT_TRAIN_PRECISION_RECALL_F1,NEW_MODEL_NEW_DIR,'plot_train_precison_recall_f1')
    plot_valid_precison_recall_f1=plot_precision_recall_f1(OUTPUT_VALID_PRECISION_RECALL_F1,NEW_MODEL_NEW_DIR,'plot_valid_precison_recall_f1')
    
    
    return NEW_MODEL_NEW_DIR
