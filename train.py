from tkinter import NE
import torch
import time
import copy
from datetime import datetime, date
from datetime import datetime as dt
from datetime import datetime, timedelta
from datetime import datetime, date
from datetime import datetime as dt
import os


    

def train_model(model, criterion, optimizer, scheduler, num_epochs,iter_spectogram_dataset_train,device,dataset_sizes):
    
    
    since=dt.strptime(str(datetime.now()), "%Y-%m-%d %H:%M:%S.%f")
    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0
    best_f1 = 0.0

    train_loss_list = []
    train_acc_list = []
    train_tn_list = []
    train_fn_list = []
    train_fp_list = []
    train_tp_list = []

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch + 1, num_epochs))
        print('-' * 10)
        model.train()  # Set model to training mode
        running_loss = 0.0
        running_corrects = 0
        #initialize tn, fn, fp and tp to zero:
        tn, fn, fp, tp = 0, 0, 0, 0  
        # Iterate over data batches:
        for i, (inputs,labels) in enumerate(iter_spectogram_dataset_train):
            inputs = inputs.to(device)
            labels = labels.to(device)
            # zero the parameter gradients at beginning of new minibatch
            optimizer.zero_grad()
            # forward
            # track history if only in train
            with torch.set_grad_enabled(True):
                outputs = model(inputs)
                _, predictions = torch.max(outputs, 1)
                loss = criterion(outputs, labels)
                #append loss to list depending on which phase we are in:
                train_loss_list.append(loss.item())
                # backward + optimize only if in training phase
                loss.backward()
                optimizer.step()
            #calculate true positive, true negative etc. for each batch:
            for pred, lab in zip(predictions, labels):
                if(pred == 0 and lab == 0):
                    tn += 1
                if(pred == 0 and lab == 1):
                        fn += 1
                if(pred == 1 and lab == 0):
                    fp += 1
                if(pred ==1 and lab ==1):
                    tp += 1 
            # update running loss and running nr of correctly predicted patches:
            running_loss += loss.item() * inputs.size(0) # loss deaveraged
            running_corrects += torch.sum(predictions == labels.data)  
            #calculate acc, precision, recall, f1 for current batch:
            correct = (predictions == labels).sum().item()
            total = labels.size(0)
            curr_acc = (correct / total)

            #add values to stat lists:
            train_acc_list.append(curr_acc)
            train_tn_list.append(tn)
            train_fn_list.append(fn)
            train_fp_list.append(fp)
            train_tp_list.append(tp)
            #nr of iterations:
            nr_iterations = len(iter_spectogram_dataset_train)
            
            #print out current results every 100 iterations:
            if (i+1) % 100 == 0: 
                print ('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}, Acc: {:.2f}%, TP: {}:, FP: {}, TN: {}, FN: {}' 
                           .format(epoch+1, num_epochs, i+1, nr_iterations, loss.item(), curr_acc * 100, tp, fp, tn, fn))
        epoch_loss = running_loss / dataset_sizes['train']
        epoch_acc = running_corrects.double() / dataset_sizes['train']
        epoch_precision = tp / (tp + fp)
        epoch_recall = tp / (tp + fn)
        epoch_f1 = 2 * ((epoch_precision * epoch_recall) / (epoch_precision + epoch_recall))

        print()
        print('Loss: {:.4f}, Accuracy: {:.4f}, Precision: {:.4f}, Recall: {:.4f}, F1: {:.4f}'.format(
            epoch_loss, epoch_acc, epoch_precision, epoch_recall, epoch_f1))
        print()
        scheduler.step()
        # calculate best accuracy after each validation epoch:
        if epoch_acc > best_acc:
            best_acc = epoch_acc
        #deep copy the model according to best f1 measure:
        if epoch_f1 > best_f1:
            best_f1 = epoch_f1
            best_model_wts = copy.deepcopy(model.state_dict())

        print()

    time_elapsed = dt.strptime(str(datetime.now()), "%Y-%m-%d %H:%M:%S.%f") - since
    print(time_elapsed)
    #print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))
    print('Best val F1: {:4f}'.format(best_f1))

    # load best model weights
    model.load_state_dict(best_model_wts)

    

    train_results={'model':model,
                'train_loss_list':train_loss_list ,
                'train_acc_list':train_acc_list,
                'train_tn_list':train_tn_list,
                'train_fn_list':train_fn_list,
                'train_fp_list':train_fp_list,
                'train_tp_list':train_tp_list}  
    return train_results
    
  

