import pandas as pd
import os
import matplotlib.pyplot as plt
import seaborn as sns
'''
as i have already described the chapiter dataset (see bachelor thesis report)
we take from the mpower dataset only 2 specific medtimepointsthe 
    - 1-"Just after parkinson medications (at your best)"
    - 2- "I don't take parkinson medications"
OUTPUT_DT_PARKINON='extracted_features_metadata/dt_parkinson.csv'  is a metadata
with the above medtimepoint
--------
replace label : take output of dt_parkinson as inputs and replace False & True 
Value in Professional_diagnosis with 0 &1
'''
OUTPUT_DT_PARKINON='extracted_features_metadata/dt_parkinson.csv'
OUTPUT_DT_PARKINON_01='extracted_features_metadata/dt_parkinson_01.csv'

PATH_DIR_PLOT_DATA='dataanalyse_plot'
try:
    os.mkdir(PATH_DIR_PLOT_DATA)
    print("Directory %s created successfully"% PATH_DIR_PLOT_DATA)
except OSError as error:
    print("Directory  %s is already exist" % PATH_DIR_PLOT_DATA)

def replace_label(OUTPUT_DT_PARKINON):
    dt_parkinson_01=pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)),
    OUTPUT_DT_PARKINON))
    dt_parkinson_01.replace({False:0,True:1},inplace=True)
    dt_parkinson_01.to_csv('extracted_features_metadata/dt_parkinson_01.csv')
    return dt_parkinson_01


def plot_dt_parkinson_01(OUTPUT_DT_PARKINON_01):
    dt_parkinson_01=pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)),
    OUTPUT_DT_PARKINON_01))
    #medtimepoint_1: I don't take Parkinson medications
    medtimepoint_1=dt_parkinson_01[(dt_parkinson_01['medTimepoint']=="I don't take Parkinson medications")]
    medtimepoint1_pro_diagnose_0=medtimepoint_1[(medtimepoint_1['professional-diagnosis']==0)]#1639
    medtimepoint1_pro_diagnose_1=medtimepoint_1[(medtimepoint_1['professional-diagnosis']==1)]#415
    #medtimepoint_1:Immediately before Parkinson medication
    medtimepoint_2=dt_parkinson_01[(dt_parkinson_01['medTimepoint']=="Immediately before Parkinson medication")]
    medtimepoint2_pro_diagnose_0=medtimepoint_2[(medtimepoint_2['professional-diagnosis']==0)]#7
    medtimepoint2_pro_diagnose_1=medtimepoint_2[(medtimepoint_2['professional-diagnosis']==1)]#1231

    fig,ax= plt.subplots(1,2,figsize=(20,5))
    sns.set_theme(style="darkgrid")

    ax[0]=sns.countplot(data=medtimepoint_1,x='professional-diagnosis',hue="professional-diagnosis",ax=ax[0])
    for p in ax[0].patches:
        ax[0].annotate('{:}'.format(p.get_height()), (p.get_x(), p.get_height()))
    
    ax[0].set_title("Verteling der professional-diagnosis für die MedtimepointIch nehme keine Parkinson-Medikamente")
    ax[0].set_xlabel('professional-diagnosis')
    ax[0].set_ylabel('Anzhal der Audioaufnahme')

    ax[1]=sns.countplot(data=medtimepoint_2,x='professional-diagnosis',hue="professional-diagnosis",ax=ax[1])
    for p in ax[1].patches:
        ax[1].annotate('{:}'.format(p.get_height()), (p.get_x(), p.get_height()))
    
    ax[1].set_title("Verteling der professional-diagnosis für die Medtimepoint Unmittelbar vor Parkinson-Medikamenten")
    ax[1].set_xlabel('professional-diagnosis')
    ax[1].set_ylabel('Anzhal der Audioaufnahme')
    plot_name='pr_diagnose_medtimepoint.png'
    plt.savefig(os.path.join(PATH_DIR_PLOT_DATA,plot_name))
    try:
        os.path.exists(os.path.join(PATH_DIR_PLOT_DATA,plot_name))
        print(" %s created successfully" % plot_name)
    except OSError as error:
        print("%s is already exist"% plot_name)

replace_label(OUTPUT_DT_PARKINON)
plot_dt_parkinson_01(OUTPUT_DT_PARKINON_01)