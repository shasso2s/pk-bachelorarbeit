import pandas as pd
import matplotlib.pyplot as plt
import numpy as np




def plot_df_train_loss_acc(OUTPUT_TRAIN_DF_LOSS_ACC,NEW_MODEL_NEW_DIR,filename):
    df_train_loss_acc=pd.read_json(OUTPUT_TRAIN_DF_LOSS_ACC)
    fig,ax=plt.subplots(1,2,figsize=(20,5))
    ax[0].plot(np.array(df_train_loss_acc['train_loss']))
    ax[1].plot(np.array(df_train_loss_acc['train_acc']))
    ax[0].set_xlabel("steps")
    ax[0].set_ylabel('train_loss')
    ax[1].set_xlabel("step")
    ax[1].set_ylabel("train_acc")
    plt.savefig(NEW_MODEL_NEW_DIR+'/'+filename+".png",dpi=200,bbox_inches='tight')


def plot_df_valid_loss_acc(OUTPUT_VALID_DF_LOSS_ACC,NEW_MODEL_NEW_DIR,filename):
    df_valid_loss_acc=pd.read_json(OUTPUT_VALID_DF_LOSS_ACC)
    fig,ax=plt.subplots(1,2,figsize=(20,5))
    ax[0].plot(np.array(df_valid_loss_acc['valid_loss']))
    ax[1].plot(np.array(df_valid_loss_acc['valid_acc']))
    ax[0].set_xlabel("steps")
    ax[0].set_ylabel('valid_loss')
    ax[1].set_xlabel("step")
    ax[1].set_ylabel('valid_acc')
    plt.savefig(NEW_MODEL_NEW_DIR+'/'+filename+".png",dpi=200,bbox_inches='tight')
    


