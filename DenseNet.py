import torch
import torch.nn as nn
import torchvision
import torchvision.models as models
Num_CLASSES=2
class DenseNet(nn.Module):
    def __init__(self, pretrained=True):
        super(DenseNet, self).__init__()
        self.model = models.densenet201(pretrained=pretrained)
        self.model.classifier = nn.Linear(1920,Num_CLASSES )
        
    def forward(self, x):
        output = self.model(x)
        return output