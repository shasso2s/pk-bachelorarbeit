import json
import pandas as pd
from utils import tensor_from_numpy
from utils import tensordataset
from utils import get_iter_spectograms_dataset
import os

OUTPUT_DIR_LOADED_TRAIN='loaded_data/dataDicTrain'
OUTPUT_DIR_LOADED_VALID='loaded_data/dataDicvalid'
OUTPUT_DIR_LOAED_TEST='loaded_data/dataDictest'

OUTPUT_DIR_DATALOADER='train_valid_test_dataloaders'

SAMPLE_RATE_TRAIN=22050
SAMPLE_RATE_VALID=22050
SAMPLE_RATE_TEST=22050
BATCH_SIZE=16


    
def iter_spectogram_dataset():
    print('----- read pickle-------')
    datadictrain = pd.read_pickle(OUTPUT_DIR_LOAED_TEST)
    datadictvalid = pd.read_pickle(OUTPUT_DIR_LOAED_TEST)
    datadictest = pd.read_pickle(OUTPUT_DIR_LOAED_TEST)
    print('tensor_from_numpy')
    train_time_series_train,train_labels=tensor_from_numpy(datadictrain['audio_time_series'],datadictrain['audio_labels'])
    valid_time_series_valid,val_labels=tensor_from_numpy(datadictvalid['audio_time_series'],datadictvalid['audio_labels'])
    test_time_series_test,test_labels=tensor_from_numpy(datadictest['audio_time_series'],datadictvalid['audio_labels'])
    print('tensordataset')
    tensor_dataset_train=tensordataset(train_time_series_train,train_labels)
    tensor_dataset_valid=tensordataset(valid_time_series_valid,val_labels)
    tensor_dataset_test=tensordataset(test_time_series_test,test_labels)

    audio_datasets = {"train": tensor_dataset_train, 
                        "valid": tensor_dataset_valid, 
                        "test": tensor_dataset_test}
    dataset_sizes = {x: len(audio_datasets[x]) for x in ["train", "valid", "test"]}
    df_dataset_sizes = pd.DataFrame.from_dict(dataset_sizes)
    df_dataset_sizes.to_json(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_DIR_DATALOADER,'dataset_sizes.json'))
    
    print('get_iter_spectograms_dataset')
    
    
    iter_spectogram_dataset_train = get_iter_spectograms_dataset(tensor_dataset_train,SAMPLE_RATE_TRAIN,BATCH_SIZE,'iter_spectogram_dataset_train')
    iter_spectogram_dataset_valid = get_iter_spectograms_dataset(tensor_dataset_valid,SAMPLE_RATE_VALID, BATCH_SIZE,'iter_spectogram_dataset_valid')
    iter_spectogram_dataset_test = get_iter_spectograms_dataset(tensor_dataset_test,SAMPLE_RATE_TEST, BATCH_SIZE,'iter_spectogram_dataset_test')

    try:
        os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_DIR_DATALOADER,'iter_spectogram_dataset_train.pth'))
        os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_DIR_DATALOADER,'iter_spectogram_dataset_valid.pth'))
        os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_DIR_DATALOADER,'iter_spectogram_dataset_test.pth'))
        print(" %s created successfully" % 'iter_spectogram_dataset_train.pth')
        print(" %s created successfully" % 'iter_spectogram_dataset_valid.pth')
        print(" %s created successfully" % 'iter_spectogram_dataset_test.pth')
    except OSError as error:
        print("%s is already exist"% 'iter_spectogram_dataset_train.pth')
        print("%s is already exist"% 'iter_spectogram_dataset_valid.pth')
        print("%s is already exist"% 'iter_spectogram_dataset_test.pth')

    print('Dataloader for Training , validation and testing is created')

x=iter_spectogram_dataset()
print(x)