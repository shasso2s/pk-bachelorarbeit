import torch
import torch.nn as nn
import torchvision.models as models

NUM_CLASSES=2
class ResNet(nn.Module):
	def __init__(self, pretrained=True):
		super(ResNet, self).__init__()
		self.model = models.resnet50(pretrained=pretrained)
		self.model.fc = nn.Linear(2048, NUM_CLASSES)
		
	def forward(self, x):
		output = self.model(x)
		return output