
from params import Params
import argparse
from DenseNet import DenseNet
from ResNet import ResNet
from Inception import Inception
from convnet import ConvNet

parser = argparse.ArgumentParser()

parser.add_argument("--config_path", type=str)
args = parser.parse_args()
params = Params(args.config_path)
device='cpu'

def choose_model():
    if params.model=='convnet':
        model=ConvNet().to(device)
    elif params.model=="densenet":
        model = DenseNet(params.dataset_name, params.pretrained).to(device)
    elif params.model=="resnet":
        model = ResNet(params.dataset_name, params.pretrained).to(device)
    elif params.model=="inception":
        model = Inception(params.dataset_name, params.pretrained).to(device) 
    return model