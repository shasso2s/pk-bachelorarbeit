import os
from os import listdir
import pandas as pd
import numpy as np
from glob import glob

OUTPUT_SYNAPSE_DATA='synapse_data'
OUTPUT_DEMOGRAFISCHE_METADATA='downloaded_metadata/demografics_datei.csv'
OUTPUT_VOICE_ACTIVITY='downloaded_metadata/voice_activity.csv'

PATH_DIR_EXTRACED_DATA='extraced_features_metadata'
try:
    os.mkdir(PATH_DIR_EXTRACED_DATA)
    print("Directory'%s created successfully"% PATH_DIR_EXTRACED_DATA)
except OSError as error:
    print("Directory '%s' is already exist"%PATH_DIR_EXTRACED_DATA)
'''
list_patient_nummer a list von Folder_num [0,999]
dt_patients_demografics: Dataframe , wich contains demografis infos of patients
dt_voice_activity: Dataframe, wich contains infos about the audiorecords

Extracted just important features from dt_patients_demografics which are [heathCode,professional_diagnose]
and store the extracted features (rows) in a new Dataframe

Extracted just important features from dt_voice_activity which are [audio_audio.m4a,healthCode,medTimepoint]
and store the extraced features(rows) in a new Dataframe

clean the new dataframe
'''
def extraced_features_metadata(OUTPUT_SYNAPSE_DATA,OUTPUT_DEMOGRAFISCHE_METADATA,OUTPUT_VOICE_ACTIVITY):
    
    dt_patients_demografics=pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_DEMOGRAFISCHE_METADATA))
    dt_voice_activity=pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_VOICE_ACTIVITY))
    
    professional_diagnosis= dt_patients_demografics['professional-diagnosis'].to_list()
    heathCode_demografics=dt_patients_demografics['healthCode'].to_list()

    audio_audio_m4a=dt_voice_activity['audio_audio.m4a'].to_list()
    heathCode_voice_activity=dt_voice_activity['healthCode'].to_list()
    medTimepoint=dt_voice_activity['medTimepoint'].to_list()

    extracted_feature_demografics=pd.DataFrame(list(zip(professional_diagnosis,heathCode_demografics)),
    columns=['professional-diagnosis','healthCode']).dropna(subset=['professional-diagnosis','healthCode'])

    extracted_feature_voice_activity=pd.DataFrame(list(zip(audio_audio_m4a,heathCode_voice_activity,medTimepoint)),
    columns=['audio_audio.m4a','healthCode','medTimepoint']).dropna(subset=['audio_audio.m4a','healthCode','medTimepoint'])

    extracted_feature_demografics.to_csv(os.path.join(PATH_DIR_EXTRACED_DATA,'extracted_feature_demografics.csv'))
    extracted_feature_voice_activity.to_csv(os.path.join(PATH_DIR_EXTRACED_DATA,'extracted_feature_voice_activity.csv'))

    extracted_feature_demografics=pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)),PATH_DIR_EXTRACED_DATA,'extracted_feature_demografics.csv'))
    extracted_feature_voice_activity=pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)),PATH_DIR_EXTRACED_DATA,'extracted_feature_voice_activity.csv'))
    

    return extracted_feature_demografics,extracted_feature_voice_activity

def metada(extracted_feature_demografics,extracted_feature_voice_activity):
    heathCode_list=[]
    medTimepoint_list=[]
    seqnummer_0_999_list=[]
    audio_path_list=[]
    prof_diagnosis_list=[]
    audio_audio_list=[]
    heathCode_does_not_exist_list=[]
    audio_audio_does_not_exist_list=[]
    
    professional_diagnosis= extracted_feature_demografics['professional-diagnosis'].to_list()
    heathCode_demografics=extracted_feature_demografics['healthCode'].to_list()

    audio_audio_m4a=extracted_feature_voice_activity['audio_audio.m4a'].to_list()
    heathCode_voice_activity=extracted_feature_voice_activity['healthCode'].to_list()
    medTimepoint=extracted_feature_voice_activity['medTimepoint'].to_list()

    list_patient_nummer=listdir(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_SYNAPSE_DATA))

    for patient_num in range(len(list_patient_nummer)):
        sequenznummer_0_999=list_patient_nummer[patient_num]
        
        size_all_audio_audio=len(glob(os.path.join(os.path.dirname(os.path.realpath(__file__)),
        OUTPUT_SYNAPSE_DATA,sequenznummer_0_999,'*'),recursive=True))
        
        for index in range(size_all_audio_audio):
            seq_audio_audio=glob(os.path.join(os.path.dirname(os.path.realpath(__file__)),
            OUTPUT_SYNAPSE_DATA,sequenznummer_0_999,'*'),recursive=True)[index].split('\\')[7]
            all_audio_path=glob(os.path.join(os.path.dirname(os.path.realpath(__file__)),
            OUTPUT_SYNAPSE_DATA,sequenznummer_0_999,seq_audio_audio,'*.wav'),recursive=True)
            audio_path=all_audio_path[0] #from // to /

            if int(seq_audio_audio) in audio_audio_m4a:

                index_seq_audio_audio=audio_audio_m4a.index(int(seq_audio_audio))                # index of seq_audio_audio in audio_audio_m4a
                heathCode_V_index_seq_audio_audio=heathCode_voice_activity[index_seq_audio_audio]   #  healthCode for the specific seq_audio_audio
                medTimePoint_index_seq_audio_audio=medTimepoint[index_seq_audio_audio]                           # medTimePoint for the specific seq_audio_audio

                if heathCode_V_index_seq_audio_audio in heathCode_demografics:
                    index_healthCode_demografics=heathCode_demografics.index(heathCode_V_index_seq_audio_audio)
                    prof_diagnose=professional_diagnosis[index_healthCode_demografics]
                    heathCode_list.append(heathCode_V_index_seq_audio_audio)
                    medTimepoint_list.append(medTimePoint_index_seq_audio_audio)
                    seqnummer_0_999_list.append(sequenznummer_0_999)
                    audio_path_list.append(audio_path)
                    prof_diagnosis_list.append(prof_diagnose)
                    audio_audio_list.append(seq_audio_audio)
            else:
                heathCode_does_not_exist_list.append(heathCode_V_index_seq_audio_audio)
                audio_audio_does_not_exist_list.append(seq_audio_audio)


    dt_parkinson=pd.DataFrame(index=np.arange(0,len(audio_audio_list))
    ,columns=['num_folder','audio_audio.m4a','healthCode','medTimepoint','professional-diagnosis','audio_path'])

    dt_parkinson['num_folder'] = seqnummer_0_999_list
    dt_parkinson['audio_audio.m4a']=audio_audio_list
    dt_parkinson['healthCode']=heathCode_list
    dt_parkinson['medTimepoint']=medTimepoint_list
    dt_parkinson['professional-diagnosis']=prof_diagnosis_list
    dt_parkinson['audio_path'] =audio_path_list
    dt_parkinson.to_csv(os.path.join(PATH_DIR_EXTRACED_DATA,'dt_parkinson.csv')) 

    return dt_parkinson












            





        
        

    


    

extracted_feature_demografics,extracted_feature_voice_activity=extraced_features_metadata(OUTPUT_SYNAPSE_DATA,OUTPUT_DEMOGRAFISCHE_METADATA,OUTPUT_VOICE_ACTIVITY)


metada(extracted_feature_demografics,extracted_feature_voice_activity)




