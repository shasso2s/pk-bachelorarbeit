import pandas as pd
import numpy as np
import os
import seaborn as sns
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split

OUTPUT_DT_PARKINSON_01='extracted_features_metadata/dt_parkinson_01.csv'
OUTPUT_DT_PARKINSON_SPLIT='dt_parkinson_split'
OUTPUT_DIR_DF_TRAIN='df_train.csv'
OUTPUT_DIR_DF_VALID='df_valid.csv'
OUTPUT_DIR_DF_TEST='df_test.csv'
PATH_DIR_PLOT_DATA='dataanalyse_plot'

try:
    os.mkdir(OUTPUT_DT_PARKINSON_SPLIT)
    print("Directory %s created successfully"% OUTPUT_DT_PARKINSON_SPLIT)
except OSError as error:
    print("Directory  %s is already exist" % OUTPUT_DT_PARKINSON_SPLIT)

def split_data_train_val_test(OUTPUT_DT_PARKINSON_01):

    dt_parkinson_01=pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_DT_PARKINSON_01))
    audio_audio_m4a = dt_parkinson_01['audio_audio.m4a'].unique()
    train_ids, test_ids = train_test_split(audio_audio_m4a, test_size=0.14, random_state=0)
    final_train_ids, val_ids = train_test_split(train_ids, test_size=0.16, random_state=0)

    df_train = dt_parkinson_01.loc[dt_parkinson_01['audio_audio.m4a'].isin(final_train_ids),:].copy()
    df_valid = dt_parkinson_01.loc[dt_parkinson_01['audio_audio.m4a'].isin(val_ids),:].copy()
    df_test = dt_parkinson_01.loc[dt_parkinson_01['audio_audio.m4a'].isin(test_ids),:].copy()
    
    try:
        
        df_train.to_csv(os.path.join(OUTPUT_DT_PARKINSON_SPLIT,OUTPUT_DIR_DF_TRAIN))
        df_valid.to_csv(os.path.join(OUTPUT_DT_PARKINSON_SPLIT,OUTPUT_DIR_DF_VALID))
        df_test.to_csv(os.path.join(OUTPUT_DT_PARKINSON_SPLIT,OUTPUT_DIR_DF_TEST))

        print("Directory'%s created successfully"% OUTPUT_DIR_DF_TRAIN)
        print("Directory'%s created successfully"% OUTPUT_DIR_DF_VALID)
        print("Directory'%s created successfully"% OUTPUT_DIR_DF_TEST)

    except OSError as error:
        print("Directory '%s' is already exist"%OUTPUT_DIR_DF_TRAIN)
        print("Directory '%s' is already exist"%OUTPUT_DIR_DF_VALID)
        print("Directory '%s' is already exist"%OUTPUT_DIR_DF_TEST)
    try:
        os.path.exists(os.path.join(OUTPUT_DT_PARKINSON_SPLIT,OUTPUT_DIR_DF_TRAIN))
        os.path.exists(os.path.join(OUTPUT_DT_PARKINSON_SPLIT,OUTPUT_DIR_DF_VALID))
        os.path.exists(os.path.join(OUTPUT_DT_PARKINSON_SPLIT,OUTPUT_DIR_DF_TEST))
        print(" %s created successfully" % OUTPUT_DIR_DF_TRAIN)
        print(" %s created successfully" % OUTPUT_DIR_DF_VALID)
        print(" %s created successfully" % OUTPUT_DIR_DF_TEST)
    except OSError as error:
        print("%s is already exist"% OUTPUT_DIR_DF_TRAIN)
        print("%s is already exist"% OUTPUT_DIR_DF_VALID)
        print("%s is already exist"% OUTPUT_DIR_DF_TEST)

    return df_train,df_valid,df_test

def plot_train_valid_test(OUTPUT_DIR_DF_TRAIN,OUTPUT_DIR_DF_VALID,OUTPUT_DIR_DF_TEST):

    df_train=pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)),
    OUTPUT_DT_PARKINSON_SPLIT,OUTPUT_DIR_DF_TRAIN))

    df_valid=pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)),
    OUTPUT_DT_PARKINSON_SPLIT,OUTPUT_DIR_DF_VALID))

    df_test=pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)),
    OUTPUT_DT_PARKINSON_SPLIT,OUTPUT_DIR_DF_TEST))


   

    fig,ax= plt.subplots(1,3,figsize=(20,5))
    sns.set_theme(style="darkgrid")

    ax[0]=sns.countplot(df_train['professional-diagnosis'],ax=ax[0])
    for p in ax[0].patches:
        ax[0].annotate('{:}'.format(p.get_height()), (p.get_x(), p.get_height()))
    
    ax[0].set_title("Verteling der professional-diagnosis in training Datensatz")
    ax[0].set_xlabel('professional-diagnosis')
    ax[0].set_ylabel('Anzhal der Audioaufnahme')

    ax[1]=sns.countplot(df_valid['professional-diagnosis'],ax=ax[1])
    for p in ax[1].patches:
        ax[1].annotate('{:}'.format(p.get_height()), (p.get_x(), p.get_height()))
    
    ax[1].set_title("Verteling der professional-diagnosis in validatin datensatz")
    ax[1].set_xlabel('professional-diagnosis')
    ax[1].set_ylabel('Anzhal der Audioaufnahme')

    ax[2]=sns.countplot(df_test['professional-diagnosis'],ax=ax[2])
    for p in ax[2].patches:
        ax[2].annotate('{:}'.format(p.get_height()), (p.get_x(), p.get_height()))
    
    ax[2].set_title("Verteling der professional-diagnosis in testing datensatz")
    ax[2].set_xlabel('professional-diagnosis')
    ax[2].set_ylabel('Anzhal der Audioaufnahme')



    plot_name='df_train_valid_test_verteilung.png'

    plt.savefig(os.path.join(PATH_DIR_PLOT_DATA,plot_name))
    try:
        os.path.exists(os.path.join(PATH_DIR_PLOT_DATA,plot_name))
        print(" %s created successfully" % plot_name)
    except OSError as error:
        print("%s is already exist"% plot_name)

df_train,df_valid,df_test=split_data_train_val_test(OUTPUT_DT_PARKINSON_01)
plot_train_valid_test(OUTPUT_DIR_DF_TRAIN,OUTPUT_DIR_DF_VALID,OUTPUT_DIR_DF_TEST)


