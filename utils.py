import torch.utils.data as data_utils
import torch
import numpy as np
from load import get_fixed_audio_len
import librosa
import os
import pandas as pd

OUTPUT_DIR_LOAED_TEST='loaded_data/dataDictest'
'''OUTPUT_DIR_DATALOADER='train_valid_test_dataloaders'

try:
     #if(os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_DIR_DATALOADER))):
        print("Directory  %s is already exist" % OUTPUT_DIR_DATALOADER)
     else:
          os.mkdir(OUTPUT_DIR_DATALOADER)
          print("Directory %s created successfully"% OUTPUT_DIR_DATALOADER)  
except OSError as error:
    print()'''

BATCH_SIZE=16

def tensordataset(tensor_audios,tensor_labels):
    return data_utils.TensorDataset(tensor_audios, tensor_labels)

def get_melspectrogram_db(wav, sr, audio_len = 4, n_fft = 2048, hop_length = 512, 
                                n_mels = 128, fmin = 20, fmax = 8300, top_db = 80):

     wav = get_fixed_audio_len(wav, sr, audio_len)
     spec = librosa.feature.melspectrogram(wav, sr = sr, n_fft = n_fft, hop_length = hop_length, 
                                            n_mels = n_mels, fmin = fmin, fmax = fmax)
     spec = librosa.power_to_db(spec, top_db = top_db)
     return spec

def get_iter_spectograms_dataset(audio_time_series, sr, BATCH_SIZE):#,filename): 
     audio_spec_image = []
     labels = []
     for wav,label in audio_time_series:
          print('wav',wav)
          print('labe',label)
          spec_img = spec_to_image(get_melspectrogram_db(wav.numpy(), sr))
          spec_img = np.expand_dims(spec_img, axis = 0)
          audio_spec_image.append(spec_img)
          labels.append(label)
     audio_spec_image = torch.Tensor(audio_spec_image)
     audio_spec_image = audio_spec_image / 255
     labels = torch.Tensor(labels).long()

     audio_spec_image = tensordataset(audio_spec_image, labels)
     iter_spectograms_dataset = data_utils.DataLoader(audio_spec_image,BATCH_SIZE)

     #curr_model_output_path=os.path.join(os.path.dirname(os.path.realpath(__file__)),OUTPUT_DIR_DATALOADER,filename+".pth")
     #torch.save(iter_spectograms_dataset,curr_model_output_path)     
     return iter_spectograms_dataset
     

def tensor_from_numpy(audios_load,labels_load):
     audios_train=torch.from_numpy(audios_load)
     labels_train=torch.from_numpy(labels_load).long()
     return audios_train,labels_train



def spec_to_image(spec):
     eps=1e-6
     mean = spec.mean()
     std = spec.std()
     spec_norm = (spec - mean) / (std + eps)
     spec_min, spec_max = spec_norm.min(), spec_norm.max()
     spec_scaled = 255 * (spec_norm - spec_min) / (spec_max - spec_min)
     spec_scaled = spec_scaled.astype(np.uint8)
     return spec_scaled

'''datadictest = pd.read_pickle(OUTPUT_DIR_LOAED_TEST)
x=datadictest['audio_time_series']
y=datadictest['audio_labels']

audios_train,labels_train=tensor_from_numpy(x,y)
print(audios_train)
print(labels_train)

x=tensordataset(audios_train,labels_train)

spec,label=get_iter_spectograms_dataset(x, 22050, 64)
print(spec)
print(label)'''
