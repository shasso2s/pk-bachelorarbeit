from tkinter import NE
import torch
import time
import copy
from datetime import datetime, date
from datetime import datetime as dt
from datetime import datetime, timedelta
from datetime import datetime, date
from datetime import datetime as dt
import os
import pandas as pd


def train_model_(model,dataloaders,dataset_sizes, criterion, optimizer, scheduler,NUM_EPOCHS,device):
    phases = ["train", "valid"]

#for tracking the loss, accuracy etc. of our model (in train and val phase):
    train_loss_list = []
    train_acc_list = []
    train_tn_list = []
    train_fn_list = []
    train_fp_list = []
    train_tp_list = []

    val_loss_list = []
    val_acc_list = []
    val_tn_list = []
    val_fn_list = []
    val_fp_list = []
    val_tp_list = []
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0
    best_f1 = 0.0

    for epoch in range(NUM_EPOCHS):
        print('Epoch {}/{}'.format(epoch + 1, NUM_EPOCHS))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in phases:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0
            running_f1 = 0.0
            
            #initialize tn, fn, fp and tp to zero:
            tn, fn, fp, tp = 0, 0, 0, 0  

            # Iterate over data batches:
            for i, (inputs,labels) in enumerate(dataloaders[phase]):
                inputs = inputs.to(device)
                labels = labels.to(device)
                

                # zero the parameter gradients at beginning of new minibatch
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, predictions = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)
                    
                    #append loss to list depending on which phase we are in:
                    if(phase == 'train'):
                        train_loss_list.append(loss.item())
                    if(phase == 'valid'):
                        val_loss_list.append(loss.item())

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()
                 
      
                #calculate true positive, true negative etc. for each batch:
                for pred, lab in zip(predictions, labels):
                    if(pred == 0 and lab == 0):
                        tn += 1
                    if(pred == 0 and lab == 1):
                        fn += 1
                    if(pred == 1 and lab == 0):
                        fp += 1
                    if(pred ==1 and lab ==1):
                        tp += 1
                
                
                # update running loss and running nr of correctly predicted patches:
                running_loss += loss.item() * inputs.size(0) # loss deaveraged
                running_corrects += torch.sum(predictions == labels.data)
                
                #calculate acc, precision, recall, f1 for current batch:
                correct = (predictions == labels).sum().item()
                total = labels.size(0)
                curr_acc = (correct / total)
                
                #add values to stat lists:
                if(phase == 'train'):
                        train_acc_list.append(curr_acc)
                        train_tn_list.append(tn)
                        train_fn_list.append(fn)
                        train_fp_list.append(fp)
                        train_tp_list.append(tp)
                
                if(phase == 'valid'):
                        
                        val_acc_list.append(curr_acc)
                        val_tn_list.append(tn)
                        val_fn_list.append(fn)
                        val_fp_list.append(fp)
                        val_tp_list.append(tp)
                #nr of iterations:
                nr_iterations = len(dataloaders[phase])
                #print out current results every 100 iterations:
                if (i+1) % 1 == 0: 
                    print ('{}, Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}, Acc: {:.2f}%, TP: {}:, FP: {}, TN: {}, FN: {}' 
                           .format(phase, epoch+1, NUM_EPOCHS, i+1, nr_iterations, loss.item(), curr_acc * 100, tp, fp, tn, fn))
            

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            epoch_precision, epoch_recall, epoch_f1=calcule_metriken(tp,fp,fn)

            print()
            print('{} Loss: {:.4f}, Accuracy: {:.4f}, Precision: {:.4f}, Recall: {:.4f}, F1: {:.4f}'.format(
                phase, epoch_loss, epoch_acc, epoch_precision, epoch_recall, epoch_f1))
            print()
            
            if phase == 'train':
                scheduler.step()
            
            # calculate best accuracy after each validation epoch:
            if phase == 'valid' and epoch_acc > best_acc:
                best_acc = epoch_acc
            
            #deep copy the model according to best f1 measure:
            if phase == 'valid' and epoch_f1 > best_f1:
                best_f1 = epoch_f1
                best_model_wts = copy.deepcopy(model.state_dict())

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))
    print('Best val F1: {:4f}'.format(best_f1))

    # load best model weights
    model.load_state_dict(best_model_wts)
    
    zipped_train_results=list(zip(train_loss_list,train_acc_list,train_tn_list,train_fn_list,train_tp_list,train_fp_list))
    zipped_valid_results=list(zip(val_loss_list,val_acc_list,val_tn_list,val_fn_list,val_tp_list,val_fp_list))
    dt_train_results=pd.DataFrame(zipped_train_results,columns=['train_loss','train_acc','train_tn','train_fn','train_tp','train_fp'])
    dt_valid_results=pd.DataFrame(zipped_valid_results,columns=['valid_loss','valid_acc','valid_tn','valid_fn','valid_tp','valid_fp'])

    return dt_train_results,dt_valid_results,model

def calcule_metriken(tp,fp,fn):
    if(tp==0):
        epoch_precision=0
        epoch_recall=0
        epoch_f1=0
    else:
        epoch_precision = tp / (tp + fp)
        epoch_recall = tp / (tp + fn)
        epoch_f1 = 2 * ((epoch_precision * epoch_recall) / (epoch_precision + epoch_recall))
    return epoch_precision,epoch_recall,epoch_f1